<?php

namespace PayBee\Http\Controllers;

use Telegram\Bot\Api;
use Illuminate\Http\Request;

class TelegramController extends Controller
{
    protected $telegram;
 
    public function __construct()
    {
        $this->telegram = new Api(env('TELEGRAM_BOT_TOKEN'));
    }
    
	//test if the telegram bot works
    public function testBot()
    {
        $response = $this->telegram->getMe();
        return $response;
    }

	//seting webhook 
	public function setWebHook()
	{
	    $serverAddress = 'https://cryptobot2.000webhostapp.com'; // we gonna use this address for setting webhook
		$url = $serverAddress.'/'. env('TELEGRAM_BOT_TOKEN') . '/webhook';
		$response = $this->telegram->setWebhook(['url' => $url]);
 
		return $response == true ? redirect()->back() : dd($response);
	}


	 public function handleRequest(Request $request) // method that telegram webhook request will be sent
    {
      Storage::put('Request.txt', $request);
      $this->text = $request['message']['text'];
      $this->chat_id = $request['message']['chat']['id'];
      
      //split the comand so can caputer variables
      $text_spilted = explode(" ", $this->text);

        switch ($text_spilted[0]) {
            case '/getBTCEquivalent':
            
                $amount = $text_spilted[1];
                $currency = $text_spilted[2];
                
                if(isset($amount) && isset($currency))
                {
                    $this->Conveter($amount,$currency);
                }
                
            
            break;
            
            case '/getUserID':
                
                break;
            default :
                
                $this->showMenu();
        }
	}
    
    

	public function showMenu($info = null) //method  where we show the available commands.
    {
       
        $message = '';
        if ($info) {
            $message .= $info . chr(10);
        }
        $message .= '/getBTCEquivalent' . chr(10);
        $message .= '/getUserID' . chr(10);

        $this->sendMessage($message);
    }

	protected function sendMessage($message, $parse_html = false) //sends chat_id and text to the telegram sendMessage API method
    {
        $data = [
            'chat_id' => $this->chat_id,
            'text' => $message,
        ];

        
              
        if ($parse_html) $data['parse_mode'] = 'HTML';
        Storage::put('SendMsg.txt', $data);
        $this->telegram->sendMessage($data);
    }

	protected function Conveter($amount,$currencyCode) // method to handel convernsion and request coindesk api
    {
	  $coinDeskResponse = (object) null;
	  if(isset($currencyCode))
	  {
	    $coinDeskResponse = json_decode(file_get_contents('https://api.coindesk.com/v1/bpi/currentprice/'.$currencyCode.'.json'),true);
	  }else 
	  {
	     //get default currency
		$results = DB::select('select currency from telegram_defaults');
	   
	     $Dcurrency;
		foreach ($results as $res) {
			 $Dcurrency = $res->currency;
		}
	
		 
		 $coinDeskResponse = json_decode(file_get_contents('https://api.coindesk.com/v1/bpi/currentprice/'.$Dcurrency.'.json'),true);
	  }
     

      $current_rate = $coinDeskResponse['bpi'][$currencyCode]['rate_float'];

      $Equivalent  =  $amount  / $current_rate;

      $data = $amount.' '.$currencyCode.' is '.$this->format($Equivalent,2).' BTC ('.round($current_rate,2).' '.$currencyCode .'- 1 BTC)';

      $this->sendMessage($data);
    }

	//function to return two degits after comma
    private function format($number) 
	{
        return preg_replace(
            '~\.[0-9]0+$~',
            null,
            sprintf('%.2f', $number)
         );
	}

}
