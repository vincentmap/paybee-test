<?php

namespace PayBee\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\TelegramDefault;
use Illuminate\Support\Facades\Schema;

class TelegramDefaultController extends Controller
{
    //
	public function store(Request $request)
    {
	     //Droping all records since our defalut currency can be only one
	    DB::table('telegram_defaults')->delete();
		
		$currency = $request->input('currency');
		
		$currency = strtoupper($currency);
		//inserting into table
		DB::insert('insert into telegram_defaults (currency) values(?)',[$currency]);

		
        return redirect('bot-config');
		

    }
}
