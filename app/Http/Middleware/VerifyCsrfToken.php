<?php

namespace PayBee\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
   
     protected $except = [
    '843792795:AAE2sKv9CCdub8JgmkQn5v_wv9Jxs8pYhaY/webhook',
    ];
}
