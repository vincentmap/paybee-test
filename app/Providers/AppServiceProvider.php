<?php

namespace PayBee\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */

	 //configure laravel to be hosted on 000webhost
    public function register() 
    {
        $this -> app -> bind('path.public', function()
{
        return base_path('public_html');
});
    }
}
