@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Configure Bot</div>

                <div class="panel-body">
                  
				   <form class="form-inline" action="/botSetDefault" method="post">
				    {{ csrf_field() }}
						  <label>Set Default Currency</label>
						  <div class="pull-right">
						  <label for="Currency">Currency</label>
						  <input type="text" class="form-control" name="currency" style="text-transform: uppercase"  id="Currency" placeholder="ZAR">
						  <button type="submit" class="btn btn-primary">Set Currency</button>
						  </div>
  
				</form>

                </div>
            </div>
        </div>
    </div>

	<!--<div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Bot Commands</div>

                <div class="panel-body">
                  
				   <form class="form-inline" action="/">

						  <label>Commands</label>
						  <div class="pull-right">
						  <label for="command">Command Name</label>
						  <input type="text" class="form-control" id="command" placeholder="/getBTCEquivalent">
						  <button type="submit" class="btn btn-primary">Add Command</button>
						  </div>
  
					</form>

					

                </div>
            </div>
        </div>
    </div>-->
</div>
@endsection
