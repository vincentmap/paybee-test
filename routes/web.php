<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('bot-config', function () {
    return view('configure');
});

Route::get('/home', 'HomeController@index');
Route::get('TestBot', 'TelegramController@testBot');
Route::get('set-hook', 'TelegramController@setWebHook');



Route::post('botSetDefault', 'TelegramDefaultController@store');